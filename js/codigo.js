var numId = 1;
var app = new Vue ({
    el : '#app',
    data : {
        lista : [
        ],
        id : '',
        nombre : '',
        descripcion : '',
        precio : '' 
    },
    methods : {
        agregarProducto: function(){
            if (this.nombre != "" && this.precio != ""){
                this.lista.push({id: numId, nombre: this.nombre, descripcion: this.descripcion, precio: this.precio});
                this.nombre="";
                this.descripcion="";
                this.precio="";
                numId++;
            }
            else{
                alert("Datos incompletos");
            }
        },
        eliminarProducto: function(id){
            this.lista.splice(id, 1);
        }
    }
})